#!/bin/bash

#
# @copyright Copyright (c) 2023-2024 WinderNet
#
# @license MIT License
#
# @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
#

# Get location of bin directory.
SCRIPT_DIR=$(realpath $(dirname "$0"))
TEST_DIR=$(realpath $SCRIPT_DIR/../test/)
VENDOR_DIR=$(realpath $SCRIPT_DIR/../vendor/)

# Execute tests.
$VENDOR_DIR/bin/phpunit $TEST_DIR/ --bootstrap $TEST_DIR/bootstrap.php --cache-result --cache-result-file $VENDOR_DIR/phpunit/.phpunit.result.cache
