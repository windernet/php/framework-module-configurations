<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

/*
 * This file should contain only local configurations (configurations, which are dependent of the environment (development, production, etc.)), which
 * should be available to all modules of the application.
 */

return (object)array ();
