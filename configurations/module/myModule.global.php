<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

/*
 * This file should contain only global configurations (configurations, which are independent of the environment (development, production, etc.)),
 * which should be available only to the module "myModule"; not to other modules of the application.
 */

return (object)array ();
