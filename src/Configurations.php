<?php

/*
 * @copyright Copyright (c) 2023-2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace WinderNet\Configurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use WinderNet\Common\Exception\InitializationException;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                    INTERFACE                                                                     \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * This interface declares basic functionalities for configurations, each implementing class must provide.
 *
 * @template ConfigurationsType - The type of the configurations (and therefor, the return type of {@see static::get()}).
 *
 * @since 0.1.0
 */
interface Configurations
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                            CONFIGURATION FUNCTIONS                                                           \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Gets all configurations of the application.
     *
     * @return ConfigurationsType - All configurations of the application.
     *
     * @throws InitializationException - If initialization failed, due to any reason.
     *
     * @since 0.1.0
     *
     * @example $configurations = Configurations::get();
     */
    public static function get(): mixed;
}
