<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace WinderNet\Configurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use function defined;

use          WinderNet\Common\Application;
use          WinderNet\Common\DependencyManager;
use          WinderNet\Common\Exception\InitializationException;
use function WinderNet\Common\Function\replace_recursive;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      CLASS                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * This class provides basic functionalities for file based configurations.
 *
 * @note All configurations are read-only. This is achieved by cloning them upon retrieval (creating a deep copy).
 *
 * @template-implements Configurations<object>
 *
 * @since 0.2.0
 */
abstract class FileBasedConfigurations implements Configurations
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                           CONFIGURATION CONSTANTS                                                            \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * The file extension of the configurations files.
     *
     * @since 0.2.0
     */
    protected const FILE_EXTENSION = '';

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                           CONFIGURATION PROPERTIES                                                           \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * All configurations of the application.
     *
     * @since 0.1.0
     */
    protected static ?object $configurations = null;

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                            CONFIGURATION FUNCTIONS                                                           \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Gets all configurations of the application.
     *
     * @param string $module - The module, for which the configurations shall be returned.
     *
     * @return object - All configurations of the application.
     *
     * @throws InitializationException - If initialization failed, due to any reason.
     *
     * @note If a module is specified (by using {@param $module}), that module's configurations will overwrite application wide configurations. If no
     * module is specified, only application wide configurations will be returned; no configurations of any module.
     *
     * @todo Rework {@param $module}'s type from string into an enumeration, after the update to PHP 8.1 has been done.
     *
     * @since 0.1.0
     *
     * @example $applicationConfigurations = JsonBasedConfigurations::get();
     * @example $moduleConfigurations = JsonBasedConfigurations::get('MyModule');
     */
    public static function get(string $module = ''): object
    {
        // Initialize application wide configurations.
        static::initializeApplication();

        // Get configurations.
        $configurations = static::clone('');

        if ('' !== $module) {
            // Initialize module configurations.
            static::initializeModule($module);

            // Overwrite application wide configurations by module configurations.
            $configurations = replace_recursive($configurations, static::clone($module));
        }

        return $configurations;
    }

    /**
     * Clones some configurations.
     *
     * @param string $module - The module, from which the configurations shall be cloned.
     *
     * @return object - The cloned configurations.
     *
     * @since 0.2.0
     */
    abstract protected static function clone(string $module): object;

    /**
     * Stores some configurations.
     *
     * @param string $module         - The module, for which the configurations shall be stored.
     * @param object $configurations - The configurations, which shall be stored.
     *
     * @since 0.2.0
     */
    abstract protected static function store(string $module, object $configurations): void;

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                           INITIALIZATION FUNCTIONS                                                           \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Initializes application wide configurations.
     *
     * @throws InitializationException - If initialization failed, due to any reason.
     *
     * @since 0.1.0
     */
    protected static function initializeApplication(): void
    {
        // Do nothing, if application wide configurations have already been initialized.
        if (null !== static::$configurations) {
            return;
        }

        // Initialize application wide configurations.
        static::$configurations = (object)array ();

        static::initializeModule('');
    }

    /**
     * Initializes module configurations for a certain module.
     *
     * @param string $module - The module, for which the module configurations will be initialized.
     *
     * @throws InitializationException - If initialization failed, due to any reason.
     *
     * @todo Rework {@param $module}'s type from string into an enumeration, after the update to PHP 8.1 has been done.
     *
     * @since 0.1.0
     */
    protected static function initializeModule(string $module): void
    {
        // Do nothing, if module configurations have already been initialized.
        if (isset (static::$configurations->{$module})) {
            return;
        }

        // Prepare path to configurations directory.
        $dm   = defined('WPF_DM') && (WPF_DM instanceof DependencyManager) ? WPF_DM : DependencyManager::class;
        $path = $dm::get(Application::class, Application::class)::path() . '/configurations/';

        if ('' !== $module) {
            $path .= 'module/' . $module . '.';
        }

        // Overwrite global by local configurations (to get application wide / module configurations) and store the result.
        static::store($module, replace_recursive(
            static::getFromFile($path . 'global' . static::FILE_EXTENSION),
            static::getFromFile($path . 'local'  . static::FILE_EXTENSION)
        ));
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                FILE FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Gets configurations from a file.
     *
     * @param string $path - The path of the file, from which the configurations shall be retrieved.
     *
     * @return object - The retrieved configurations.
     *
     * @throws InitializationException - If retrieving the configurations failed, due to any reason.
     *
     * @since 0.1.0
     */
    abstract protected static function getFromFile(string $path): object;
}
