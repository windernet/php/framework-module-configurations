<?php

/*
 * @copyright Copyright (c) 2023-2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace WinderNet\Configurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use          JsonException;
use const    JSON_ERROR_NONE;
use function file_get_contents;
use function is_file;
use function is_object;
use function is_readable;
use function json_decode;
use function json_encode;
use function json_last_error;
use function json_last_error_msg;

use WinderNet\Common\Exception\InitializationException;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      CLASS                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * This class provides functionalities for JSON based configurations.
 *
 * @note All configurations are read-only. This is achieved by cloning them upon retrieval (creating a deep copy).
 *
 * @template-implements FileBasedConfigurations<string>
 *
 * @since 0.1.0
 */
class JsonBasedConfigurations extends FileBasedConfigurations
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                           CONFIGURATION CONSTANTS                                                            \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * The file extension of the configurations files.
     *
     * @since 0.2.0
     */
    protected const FILE_EXTENSION = '.json';

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                            CONFIGURATION FUNCTIONS                                                           \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Clones some configurations.
     *
     * @param string $module - The module, from which the configurations shall be cloned.
     *
     * @return object - The cloned configurations.
     *
     * @since 0.2.0
     */
    protected static function clone(string $module): object
    {
        return json_decode(static::$configurations->{$module});
    }

    /**
     * Stores some configurations.
     *
     * @param string $module         - The module, for which the configurations shall be stored.
     * @param object $configurations - The configurations, which shall be stored.
     *
     * @todo Rework {@param $module}'s type from string into an enumeration, after the update to PHP 8.1 has been done.
     *
     * @since 0.2.0
     */
    protected static function store(string $module, object $configurations): void
    {
        static::$configurations->{$module} = json_encode($configurations);
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                FILE FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Gets configurations from a file.
     *
     * @param string $path - The path of the file, from which the configurations shall be retrieved.
     *
     * @return object - The retrieved configurations.
     *
     * @throws InitializationException - If retrieving the configurations failed, due to any reason.
     *
     * @since 0.1.0
     */
    protected static function getFromFile(string $path): object
    {
        // Make sure configurations file does exist and is readable.
        if (!is_file($path) || !is_readable($path)) {
            throw new InitializationException('The file at "' . $path . '" does either not exist or is not readable');
        }

        // Try to get configurations from file.
        $configurations = file_get_contents($path);

        if (false === $configurations) {
            throw new InitializationException('Unable to get content from "' . $path . '".');
        }

        // Make sure configurations contain valid JSON.
        $configurations = json_decode($configurations);

        if (JSON_ERROR_NONE !== json_last_error()) { // @todo Replace with json_validate(), after migration to PHP 8.3.
            throw new InitializationException(
                'The file at "' . $path . '" does not contain valid JSON.',
                previous: new JsonException(json_last_error_msg(), json_last_error())
            );
        }

        // Validate configurations.
        if (!is_object($configurations)) {
            throw new InitializationException('The file at "' . $path . '" does not provide a JSON object.');
        }

        // Return retrieved configurations.
        return $configurations;
    }
}
