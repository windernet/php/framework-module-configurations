<?php

/*
 * @copyright Copyright (c) 2023-2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace WinderNet\Configurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use          Throwable;
use function is_file;
use function is_object;
use function is_readable;

use WinderNet\Common\Exception\InitializationException;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      CLASS                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * This class provides functionalities for PHP based configurations.
 *
 * @note All configurations are read-only. This is achieved by cloning them upon retrieval (creating a deep copy).
 *
 * @template-implements FileBasedConfigurations<object>
 *
 * @since 0.1.0
 */
class PhpBasedConfigurations extends FileBasedConfigurations
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                           CONFIGURATION CONSTANTS                                                            \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * The file extension of the configurations files.
     *
     * @since 0.2.0
     */
    protected const FILE_EXTENSION = '.php';

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                            CONFIGURATION FUNCTIONS                                                           \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Clones some configurations.
     *
     * @param string $module - The module, from which the configurations shall be cloned.
     *
     * @return object - The cloned configurations.
     *
     * @since 0.2.0
     */
    protected static function clone(string $module): object
    {
        return clone static::$configurations->{$module};
    }

    /**
     * Stores some configurations.
     *
     * @param string $module         - The module, for which the configurations shall be stored.
     * @param object $configurations - The configurations, which shall be stored.
     *
     * @todo Rework {@param $module}'s type from string into an enumeration, after the update to PHP 8.1 has been done.
     *
     * @since 0.2.0
     */
    protected static function store(string $module, object $configurations): void
    {
        static::$configurations->{$module} = $configurations;
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                FILE FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Gets configurations from a file.
     *
     * @param string $path - The path of the file, from which the configurations shall be retrieved.
     *
     * @return object - The retrieved configurations.
     *
     * @throws InitializationException - If retrieving the configurations failed, due to any reason.
     *
     * @since 0.1.0
     */
    protected static function getFromFile(string $path): object
    {
        // Make sure configurations file does exist and is readable.
        if (!is_file($path) || !is_readable($path)) {
            throw new InitializationException('The file at "' . $path . '" does either not exist or is not readable');
        }

        // Try to get configurations from file.
        try {
            $configurations = require ($path);
        } catch (Throwable $throwable) {
            throw new InitializationException('Requiring the file at "' . $path . '" failed.', previous: $throwable);
        }

        // Validate configurations.
        if (!is_object($configurations)) {
            throw new InitializationException('The file at "' . $path . '" does not provide a PHP object.');
        }

        // Return retrieved configurations.
        return $configurations;
    }
}
