<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Configurations\FileBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use          ReflectionException;
use function defined;
use function exec;
use function file_put_contents;

use Test\WinderNet\Configurations\ResetTrait;

use WinderNet\Common\Application;
use WinderNet\Common\DependencyManager;
use WinderNet\Configurations\FileBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      TRAIT                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * This trait provides some helping functionalities for tests, which use {@see FileBasedConfigurations}.
 */
trait TestHelper
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                 USED TRAITS                                                                  \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    use ResetTrait { // Implies {@see ReflectionTrait}
        setUpBeforeClass   as ___setUpBeforeClass;
        tearDown           as ___tearDown;
        tearDownAfterClass as ___tearDownAfterClass;
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                               TEST PROPERTIES                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * The path to the application directory.
     */
    protected static ?string $applicationPath = null;

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                              PHP UNIT FUNCTIONS                                                              \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Prepares all upcoming tests of this test class, before their execution.
     *
     * @throws ReflectionException - If {@see static::$methodName} is invalid.
     */
    public static function setUpBeforeClass(): void
    {
        // Set up traits.
        static::___setupBeforeClass();

        // Get application path.
        $dm                      = defined('WPF_DM') ? WPF_DM : DependencyManager::class;
        static::$applicationPath = $dm::get(Application::class, Application::class)::path();

        // Backup configurations.
        exec('cp -r ' . static::$applicationPath . '/configurations/ ' . static::$applicationPath . '/configurations_backup/');
    }

    /**
     * Cleans up after all tests of this test class have been executed.
     */
    public static function tearDownAfterClass(): void
    {
        // Remove backed up configurations.
        exec('rm -r ' . static::$applicationPath . '/configurations_backup/');

        // Tear down traits.
        static::___tearDownAfterClass();
    }

    /**
     * Cleans up after a test has been executed.
     */
    public function tearDown(): void
    {
        // Reset configurations.
        exec('rm -r ' . static::$applicationPath . '/configurations/');
        exec('cp -r ' . static::$applicationPath . '/configurations_backup/ ' . static::$applicationPath . '/configurations/');

        // Tear down traits.
        static::___tearDown();
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                               HELPER FUNCTIONS                                                               \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Gets some invalid file content.
     *
     * @return string - Some invalid file content.
     */
    abstract protected static function getInvalidFileContent(): string;

    /**
     * Gets some configurations, in stored representation.
     *
     * @param object $configurations - The configurations, from which the stored representation shall be returned.
     *
     * @return mixed - The configurations, in stored representation.
     */
    abstract protected static function getStoredConfigurations(object $configurations): mixed;

    /**
     * Gets some valid file content.
     *
     * @param mixed $configurations - The configurations, which shall be put into the file content.
     *
     * @return string - Some valid file content.
     */
    abstract protected static function getValidFileContent(mixed $configurations): string;

    /**
     * Overwrites the content of a file.
     *
     * @param string $path           - The relative path to the file, of which the content shall be overwritten.
     * @param string $configurations - The configurations, which shall be used to overwrite the file.
     */
    protected static function overwriteFileContent(string $path, mixed $configurations): void
    {
        file_put_contents(
            static::$applicationPath . $path . static::$class->getConstant('FILE_EXTENSION'),
            static::getValidFileContent($configurations)
        );
    }
}
