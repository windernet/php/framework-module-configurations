<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Configurations\FileBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use ReflectionException;

use PHPUnit\Framework\TestCase;

use WinderNet\Configurations\FileBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                       TEST                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * @see FileBasedConfigurations::clone()
 */
abstract class cloneTestCase extends TestCase
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                 USED TRAITS                                                                  \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    use TestHelper {
        setUpBeforeClass as _setUpBeforeClass;
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                              PHP UNIT FUNCTIONS                                                              \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Prepares all upcoming tests of this test class, before their execution.
     *
     * @throws ReflectionException - If the function, which shall be tested, is not implemented.
     */
    public static function setUpBeforeClass(): void
    {
        // Set up traits.
        static::$methodName = 'clone';

        static::_setUpBeforeClass();
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                TEST FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * @throws ReflectionException
     */
    public function testMustReturnCorrectConfigurations(): void
    {
        // Prepare test.
        $configurations = (object)array ('correct' => true);

        static::$class->setStaticPropertyValue('configurations', (object)array ('module' => static::getStoredConfigurations($configurations)));

        // Run and assert test.
        $this->assertEquals($configurations, static::$method->invoke(null, 'module'));
    }

    /**
     * @throws ReflectionException
     */
    public function testMustReturnClonedConfigurations(): void
    {
        // Prepare test.
        $configurations = (object)array ('module' => static::getStoredConfigurations((object)array ('cloned' => true)));

        static::$class->setStaticPropertyValue('configurations', $configurations);

        // Run test.
        $clonedConfigurations         = static::$method->invoke(null, 'module');
        $clonedConfigurations->cloned = false;

        // Assert test.
        $this->assertEquals($configurations, static::$class->getStaticPropertyValue('configurations'));
    }
}
