<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Configurations\FileBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use          ReflectionException;
use function chmod;
use function file_put_contents;
use function is_file;
use function tempnam;
use function unlink;

use PHPUnit\Framework\TestCase;

use WinderNet\Common\Exception\InitializationException;
use WinderNet\Configurations\FileBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                       TEST                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * @see FileBasedConfigurations::getFromFile()
 */
abstract class getFromFileTestCase extends TestCase
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                 USED TRAITS                                                                  \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    use TestHelper {
        setUp            as _setUp;
        setUpBeforeClass as _setUpBeforeClass;
        tearDown         as _tearDown;
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                               TEST PROPERTIES                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * A path, which points to a temporary test file.
     */
    protected static string $path = '';

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                              PHP UNIT FUNCTIONS                                                              \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Prepares all upcoming tests of this test class, before their execution.
     *
     * @throws ReflectionException - If the function, which shall be tested, is not implemented.
     */
    public static function setUpBeforeClass(): void
    {
        // Set up traits.
        static::$methodName = 'getFromFile';

        static::_setUpBeforeClass();
    }

    /**
     * Prepares a test, before its execution.
     */
    public function setUp(): void
    {
        // Set up traits.
        $this->_setUp();

        // Create temporary test file.
        static::$path = tempnam('/tmp', '');
    }

    /**
     * Cleans up after a test has been executed.
     */
    public function tearDown(): void
    {
        // Delete temporary test file.
        if (is_file(static::$path)) {
            unlink(static::$path);
        }

        static::$path = '';

        // Tear down traits.
        $this->_tearDown();
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                TEST FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * @throws ReflectionException
     */
    public function testMustThrowExceptionIfPathIsNotPointingToAFile(): void
    {
        // Prepare test.
        unlink(static::$path);
        $this->expectException(InitializationException::class);

        // Run test.
        static::$method->invoke(null, static::$path);
    }

    /**
     * @throws ReflectionException
     */
    public function testMustThrowExceptionIfFileIsNotReadable(): void
    {
        // Prepare test.
        chmod(static::$path, 0222);
        $this->expectException(InitializationException::class);

        // Run test.
        static::$method->invoke(null, static::$path);
    }

    /**
     * @throws ReflectionException
     */
    public function testMustThrowExceptionIfFileContentIsInvalid(): void
    {
        // Prepare test.
        file_put_contents(static::$path, static::getInvalidFileContent());
        $this->expectException(InitializationException::class);

        // Run test.
        static::$method->invoke(null, static::$path);
    }

    /**
     * @throws ReflectionException
     */
    public function testMustThrowExceptionIfFileContainsNoObject(): void
    {
        // Prepare test.
        $count  = 0;
        $values = array (null, false, true, 0, '', array ());

        foreach ($values as $value) {
            file_put_contents(static::$path, static::getValidFileContent($value));

            try {
                // Run test.
                static::$method->invoke(null, static::$path);
            } catch (InitializationException) {
                ++$count;
            }
        }

        // Assert test.
        $this->assertCount($count, $values);
    }

    /**
     * @throws ReflectionException
     */
    public function testMustReturnRetrievedConfigurations(): void
    {
        // Prepare test.
        $configurations = (object)array ('valid' => true);

        file_put_contents(static::$path, static::getValidFileContent($configurations));

        // Run and assert test.
        $this->assertEquals($configurations, static::$method->invoke(null, static::$path));
    }
}
