<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Configurations\FileBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use function call_user_func;

use PHPUnit\Framework\TestCase;

use WinderNet\Configurations\FileBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                       TEST                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * @see FileBasedConfigurations::get()
 */
abstract class getTestCase extends TestCase
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                 USED TRAITS                                                                  \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    use TestHelper;

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                TEST FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     */
    public function testMustInitializeApplicationWideConfigurations(): void
    {
        // Run test.
        call_user_func(static::$className . '::get');

        // Assert test.
        $configurations = static::$class->getStaticPropertyValue('configurations');

        $this->assertNotNull($configurations);
        $this->assertIsObject($configurations);
        $this->assertObjectHasProperty('', $configurations);
        $this->assertNotNull($configurations->{''});
    }

    /**
     */
    public function testMustReturnApplicationWideConfigurationsIfNoModuleIsSpecified(): void
    {
        // Prepare test.
        static::overwriteFileContent('/configurations/global'                , (object)array ('APPLICATION' => true, 'MODULE' => false));
        static::overwriteFileContent('/configurations/local'                 , (object)array ());
        static::overwriteFileContent('/configurations/module/myModule.global', (object)array ('APPLICATION' => true, 'MODULE' => true));
        static::overwriteFileContent('/configurations/module/myModule.local' , (object)array ());

        // Run and assert test.
        $this->assertEquals((object)array ('APPLICATION' => true, 'MODULE' => false), call_user_func(static::$className . '::get'));
    }

    /**
     */
    public function testMustOverwriteConfigurationsInFollowingOrder_GlobalApplicationWide_LocalApplicationWide(): void
    {
        // Prepare test.
        static::overwriteFileContent('/configurations/global', (object)array ('APPLICATION' => (object)array ('SCOPE' => 'GLOBAL')));
        static::overwriteFileContent('/configurations/local' , (object)array ('APPLICATION' => (object)array ('SCOPE' => 'LOCAL')));

        // Run and assert test.
        $this->assertEquals((object)array ('APPLICATION' => (object)array ('SCOPE' => 'LOCAL')), call_user_func(static::$className . '::get'));
    }

    /**
     */
    public function testMustInitializeModuleConfigurationsIfAModuleIsSpecified(): void
    {
        // Run test.
        call_user_func(static::$className . '::get', 'myModule');

        // Assert test.
        $configurations = static::$class->getStaticPropertyValue('configurations');

        $this->assertNotNull($configurations);
        $this->assertIsObject($configurations);
        $this->assertObjectHasProperty('myModule', $configurations);
        $this->assertNotNull($configurations->myModule);
    }

    /**
     */
    public function testMustReturnModuleConfigurationsIfAModuleIsSpecified(): void
    {
        // Prepare test.
        static::overwriteFileContent('/configurations/global'                , (object)array ('APPLICATION' => true, 'MODULE' => false));
        static::overwriteFileContent('/configurations/local'                 , (object)array ());
        static::overwriteFileContent('/configurations/module/myModule.global', (object)array ('APPLICATION' => true, 'MODULE' => true));
        static::overwriteFileContent('/configurations/module/myModule.local' , (object)array ());

        // Run and assert test.
        $this->assertEquals((object)array ('APPLICATION' => true, 'MODULE' => true), call_user_func(static::$className . '::get', 'myModule'));
    }

    /**
     */
    public function testMustOverwriteConfigurationsInFollowingOrder_GlobalApplicationWide_LocalApplicationWide_GlobalModule_LocalModule(): void
    {
        // Prepare test.
        static::overwriteFileContent(
            '/configurations/global',
            (object)array ('APPLICATION' => (object)array ('SCOPE' => 'GLOBAL'), 'MODULE' => (object)array ())
        );
        static::overwriteFileContent(
            '/configurations/local',
            (object)array ('APPLICATION' => (object)array ('SCOPE' => 'LOCAL'), 'MODULE' => (object)array ())
        );
        static::overwriteFileContent(
            '/configurations/module/myModule.global',
            (object)array ('APPLICATION' => (object)array (), 'MODULE' => (object)array ('SCOPE' => 'GLOBAL'))
        );
        static::overwriteFileContent(
            '/configurations/module/myModule.local',
            (object)array ('APPLICATION' => (object)array (), 'MODULE' => (object)array ('SCOPE' => 'LOCAL'))
        );

        // Run and assert test.
        $this->assertEquals(
            (object)array ('APPLICATION' => (object)array ('SCOPE' => 'LOCAL'), 'MODULE' => (object)array ('SCOPE' => 'LOCAL')),
            call_user_func(static::$className . '::get', 'myModule')
        );
    }
}
