<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Configurations\FileBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use ReflectionException;

use PHPUnit\Framework\TestCase;

use WinderNet\Configurations\FileBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                       TEST                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * @see FileBasedConfigurations::initializeApplication()
 */
abstract class initializeApplicationTestCase extends TestCase
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                 USED TRAITS                                                                  \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    use TestHelper {
        setUpBeforeClass as _setUpBeforeClass;
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                              PHP UNIT FUNCTIONS                                                              \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Prepares all upcoming tests of this test class, before their execution.
     *
     * @throws ReflectionException - If the function, which shall be tested, is not implemented.
     */
    public static function setUpBeforeClass(): void
    {
        // Set up traits.
        static::$methodName = 'initializeApplication';

        static::_setUpBeforeClass();
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                TEST FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * @throws ReflectionException
     */
    public function testMustInitializeApplicationWideConfigurationsIfTheyAreNotInitialized(): void
    {
        // Run test.
        static::$method->invoke(null);

        // Assert test.
        $configurations = static::$class->getStaticPropertyValue('configurations');

        $this->assertNotNull($configurations);
        $this->assertIsObject($configurations);
        $this->assertObjectHasProperty('', $configurations);
        $this->assertNotNull($configurations->{''});
    }

    /**
     * @throws ReflectionException
     */
    public function testMustNotReinitializeApplicationWideConfigurationsIfTheyAreAlreadyInitialized(): void
    {
        // Prepare test.
        static::overwriteFileContent('/configurations/global', (object)array ('EQUAL' => true));
        static::$method->invoke(null);

        $configurations = static::$class->getStaticPropertyValue('configurations');

        static::overwriteFileContent('/configurations/global', (object)array ('EQUAL' => false));

        // Run test.
        static::$method->invoke(null);

        // Assert test.
        $this->assertEquals($configurations, static::$class->getStaticPropertyValue('configurations'));
    }

    /**
     * @throws ReflectionException
     */
    public function testMustOverwriteGlobalApplicationWideConfigurationsWithLocalApplicationWideConfigurations(): void
    {
        // Prepare test.
        static::overwriteFileContent('/configurations/global', (object)array ('GLOBAL' => true, 'LOCAL' => false));
        static::overwriteFileContent('/configurations/local' , (object)array ('LOCAL' => true));

        // Run test.
        static::$method->invoke(null);

        // Assert test.
        $this->assertEquals(
            static::getStoredConfigurations((object)array ('GLOBAL' => true, 'LOCAL' => true)),
            static::$class->getStaticPropertyValue('configurations')->{''}
        );
    }
}
