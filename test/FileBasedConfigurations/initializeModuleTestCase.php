<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Configurations\FileBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use          ReflectionException;
use function call_user_func;

use PHPUnit\Framework\TestCase;

use WinderNet\Configurations\FileBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                       TEST                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * @see FileBasedConfigurations::initializeModule()
 */
abstract class initializeModuleTestCase extends TestCase
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                 USED TRAITS                                                                  \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    use TestHelper {
        setUp            as _setUp;
        setUpBeforeClass as _setUpBeforeClass;
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                              PHP UNIT FUNCTIONS                                                              \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Prepares all upcoming tests of this test class, before their execution.
     *
     * @throws ReflectionException - If the function, which shall be tested, is not implemented.
     */
    public static function setUpBeforeClass(): void
    {
        // Set up traits.
        static::$methodName = 'initializeModule';

        static::_setUpBeforeClass();
    }

    /**
     * Prepares a test, before its execution.
     */
    public function setUp(): void
    {
        // Set up traits.
        $this->_setUp();

        // Initialize application wide configurations.
        call_user_func(static::$className . '::get');
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                TEST FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * @throws ReflectionException
     */
    public function testMustInitializeModuleConfigurationsIfTheyAreNotInitialized(): void
    {
        // Run test.
        static::$method->invoke(null, 'myModule');

        // Assert test.
        $configurations = static::$class->getStaticPropertyValue('configurations');

        $this->assertNotNull($configurations);
        $this->assertIsObject($configurations);
        $this->assertObjectHasProperty('myModule', $configurations);
        $this->assertNotNull($configurations->myModule);
    }

    /**
     * @throws ReflectionException
     */
    public function testMustNotReinitializeModuleConfigurationsIfTheyAreAlreadyInitialized(): void
    {
        // Prepare test.
        static::overwriteFileContent('/configurations/module/myModule', (object)array ('EQUAL' => true));
        static::$method->invoke(null, 'myModule');

        $configurations = static::$class->getStaticPropertyValue('configurations');

        static::overwriteFileContent('/configurations/module/myModule', (object)array ('EQUAL' => false));

        // Run test.
        static::$method->invoke(null, 'myModule');

        // Assert test.
        $this->assertEquals($configurations, static::$class->getStaticPropertyValue('configurations'));
    }

    /**
     * @throws ReflectionException
     */
    public function testMustOverwriteGlobalModuleConfigurationsWithLocalModuleConfigurations(): void
    {
        // Prepare test.
        static::overwriteFileContent('/configurations/module/myModule.global', (object)array ('GLOBAL' => true, 'LOCAL' => false));
        static::overwriteFileContent('/configurations/module/myModule.local' , (object)array ('LOCAL' => true));

        // Run test.
        static::$method->invoke(null, 'myModule');

        // Assert test.
        $this->assertEquals(
            static::getStoredConfigurations((object)array ('GLOBAL' => true,'LOCAL' => true)),
            static::$class->getStaticPropertyValue('configurations')->myModule
        );
    }
}
