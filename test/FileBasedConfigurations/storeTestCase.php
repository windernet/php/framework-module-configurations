<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Configurations\FileBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use ReflectionException;

use PHPUnit\Framework\TestCase;

use WinderNet\Configurations\FileBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                       TEST                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * @see FileBasedConfigurations::store()
 */
abstract class storeTestCase extends TestCase
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                 USED TRAITS                                                                  \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    use TestHelper {
        setUp            as _setUp;
        setUpBeforeClass as _setUpBeforeClass;
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                              PHP UNIT FUNCTIONS                                                              \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Prepares all upcoming tests of this test class, before their execution.
     *
     * @throws ReflectionException - If the function, which shall be tested, is not implemented.
     */
    public static function setUpBeforeClass(): void
    {
        // Set up traits.
        static::$methodName = 'store';

        static::_setUpBeforeClass();
    }

    /**
     * Prepares a test, before its execution.
     */
    public function setUp(): void
    {
        // Set up traits.
        $this->_setUp();

        // Initialize configurations.
        static::$class->setStaticPropertyValue('configurations', (object)array ());
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                TEST FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * @throws ReflectionException
     */
    public function testMustStoreConfigurations(): void
    {
        // Run test.
        static::$method->invoke(null, 'store', (object)array ('stored' => true));

        // Assert test.
        $configurations = static::$class->getStaticPropertyValue('configurations');

        $this->assertNotNull($configurations);
        $this->assertIsObject($configurations);
        $this->assertObjectHasProperty('store', $configurations);
        $this->assertEquals(static::getStoredConfigurations((object)array ('stored' => true)), $configurations->store);
    }
}
