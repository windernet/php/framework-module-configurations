<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Configurations\JsonBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use function json_encode;

use WinderNet\Configurations\JsonBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      TRAIT                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * This trait provides some helping functionalities for tests, which use {@see JsonBasedConfigurations}.
 */
trait TestHelper
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                               HELPER FUNCTIONS                                                               \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Gets some invalid file content.
     *
     * @return string - Some invalid file content.
     */
    protected static function getInvalidFileContent(): string
    {
        return '{';
    }

    /**
     * Gets some configurations, in stored representation.
     *
     * @param object $configurations - The configurations, from which the stored representation shall be returned.
     *
     * @return string - The configurations, in stored representation.
     */
    protected static function getStoredConfigurations(object $configurations): string
    {
        return json_encode($configurations);
    }

    /**
     * Gets some valid file content.
     *
     * @param mixed $configurations - The configurations, which shall be put into the file content.
     *
     * @return string - Some valid file content.
     */
    protected static function getValidFileContent(mixed $configurations): string
    {
        return json_encode($configurations);
    }
}
