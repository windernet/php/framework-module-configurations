<?php

/*
 * @copyright Copyright (c) 2023-2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Configurations\JsonBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use Test\WinderNet\Configurations\FileBasedConfigurations\getTestCase;

use WinderNet\Configurations\JsonBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                       TEST                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * @see JsonBasedConfigurations::get()
 */
class getTest extends getTestCase
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                 USED TRAITS                                                                  \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    use TestHelper;

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                TEST FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     */
    public function testMustInitializeApplicationWideConfigurations(): void
    {
        // Call parent test.
        parent::testMustInitializeApplicationWideConfigurations();

        // Assert test.
        $configurations = self::$class->getStaticPropertyValue('configurations');

        $this->assertIsString($configurations->{''});
    }

    /**
     */
    public function testMustInitializeModuleConfigurationsIfAModuleIsSpecified(): void
    {
        // Call parent test.
        parent::testMustInitializeModuleConfigurationsIfAModuleIsSpecified();

        // Assert test.
        $configurations = self::$class->getStaticPropertyValue('configurations');

        $this->assertIsString($configurations->myModule);
    }
}
