<?php

/*
 * @copyright Copyright (c) 2023-2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Configurations\JsonBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use Test\WinderNet\Configurations\FileBasedConfigurations\initializeApplicationTestCase;

use WinderNet\Configurations\JsonBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                       TEST                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * @see JsonBasedConfigurations::initializeApplication()
 */
class initializeApplicationTest extends initializeApplicationTestCase
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                 USED TRAITS                                                                  \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    use TestHelper;

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                TEST FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    public function testMustInitializeApplicationWideConfigurationsIfTheyAreNotInitialized(): void
    {
        // Call parent test.
        parent::testMustInitializeApplicationWideConfigurationsIfTheyAreNotInitialized();

        // Assert test.
        $configurations = self::$class->getStaticPropertyValue('configurations');

        $this->assertIsString($configurations->{''});
    }
}
