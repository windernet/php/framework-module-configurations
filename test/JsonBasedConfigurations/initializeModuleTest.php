<?php

/*
 * @copyright Copyright (c) 2023-2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Configurations\JsonBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use ReflectionException;

use Test\WinderNet\Configurations\FileBasedConfigurations\initializeModuleTestCase;

use WinderNet\Configurations\JsonBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                       TEST                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * @see JsonBasedConfigurations::initializeModule()
 */
class initializeModuleTest extends initializeModuleTestCase
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                 USED TRAITS                                                                  \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    use TestHelper;

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                TEST FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * @throws ReflectionException
     */
    public function testMustInitializeModuleConfigurationsIfTheyAreNotInitialized(): void
    {
        // Call parent test.
        parent::testMustInitializeModuleConfigurationsIfTheyAreNotInitialized();

        // Assert test.
        $configurations = self::$class->getStaticPropertyValue('configurations');

        $this->assertIsString($configurations->myModule);
    }
}
