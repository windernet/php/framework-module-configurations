<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Configurations\PhpBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use const    PHP_EOL;
use function is_array;
use function is_bool;
use function is_numeric;
use function is_object;
use function is_string;
use function rtrim;

use WinderNet\Configurations\PhpBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      TRAIT                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * This trait provides some helping functionalities for tests, which use {@see PhpBasedConfigurations}.
 */
trait TestHelper
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                               HELPER FUNCTIONS                                                               \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Gets some invalid file content.
     *
     * @return string - Some invalid file content.
     */
    protected static function getInvalidFileContent(): string
    {
        return '<?php' . PHP_EOL . PHP_EOL . 'return noll;' . PHP_EOL;
    }

    /**
     * Gets some configurations, in stored representation.
     *
     * @param object $configurations - The configurations, from which the stored representation shall be returned.
     *
     * @return object - The configurations, in stored representation.
     */
    protected static function getStoredConfigurations(object $configurations): object
    {
        return $configurations;
    }

    /**
     * Gets some valid file content.
     *
     * @param mixed $configurations - The configurations, which shall be put into the file content.
     *
     * @return string - Some valid file content.
     */
    protected static function getValidFileContent(mixed $configurations): string
    {
        return '<?php' . PHP_EOL . PHP_EOL . 'return ' . self::configurationsToString($configurations) . ';' . PHP_EOL;
    }

    /**
     * Returns a string representation of the configurations.
     *
     * @param mixed $configurations - The configurations, of which a string representation shall be returned.
     *
     * @return string - A string representation of {@param $configurations}.
     */
    private static function configurationsToString(mixed $configurations): string
    {
        switch (true) {
            case is_object($configurations):
                $object = '(object)array (';

                foreach ($configurations as $key => $value) {
                    $object .= '"' . $key . '" => ' . self::configurationsToString($value) . ',';
                }

                return rtrim($object, ',') . ')';
            // ----------------------------------------------------------------------------------------------------
            case is_array($configurations):
                $array = 'array (';

                foreach ($configurations as $index => $element) {
                    $array .= $index . ' => ' . self::configurationsToString($element) . ',';
                }

                return rtrim($array, ',') . ')';
            // ----------------------------------------------------------------------------------------------------
            case is_string($configurations):
                return '"' . $configurations . '"';
            // ----------------------------------------------------------------------------------------------------
            case is_numeric($configurations):
                return $configurations . '';
            // ----------------------------------------------------------------------------------------------------
            case is_bool($configurations):
                return $configurations ? 'true' : 'false';
            // ----------------------------------------------------------------------------------------------------
            default:
                return 'null';
        }
    }
}
