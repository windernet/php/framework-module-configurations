<?php

/*
 * @copyright Copyright (c) 2023-2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Configurations\PhpBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use          Error;
use          ReflectionException;
use function file_put_contents;

use Test\WinderNet\Configurations\FileBasedConfigurations\getFromFileTestCase;

use WinderNet\Common\Exception\InitializationException;
use WinderNet\Configurations\PhpBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                       TEST                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * @see PhpBasedConfigurations::getFromFile()
 */
class getFromFileTest extends getFromFileTestCase
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                 USED TRAITS                                                                  \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    use TestHelper;

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                TEST FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * @throws ReflectionException
     */
    public function testMustProvidePreviousExceptionIfFileContentIsInvalid(): void
    {
        // Prepare test.
        file_put_contents(self::$path, self::getInvalidFileContent());

        try {
            // Run test.
            self::$method->invoke(null, self::$path);
        } catch (InitializationException $exception) {
            $exception = $exception->getPrevious();
        }

        // Assert test.
        $this->assertNotNull($exception);
        $this->assertInstanceOf(Error::class, $exception);
    }
}
