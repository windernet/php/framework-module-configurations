<?php

/*
 * @copyright Copyright (c) 2023-2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Configurations\PhpBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use Test\WinderNet\Configurations\FileBasedConfigurations\getTestCase;

use WinderNet\Configurations\PhpBasedConfigurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                       TEST                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * @see PhpBasedConfigurations::get()
 */
class getTest extends getTestCase
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                 USED TRAITS                                                                  \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    use TestHelper;

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                TEST FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     */
    public function testMustInitializeApplicationWideConfigurations(): void
    {
        // Call parent test.
        parent::testMustInitializeApplicationWideConfigurations();

        // Assert test.
        $configurations = self::$class->getStaticPropertyValue('configurations');

        $this->assertIsObject($configurations->{''});
    }

    /**
     */
    public function testMustInitializeModuleConfigurationsIfAModuleIsSpecified(): void
    {
        // Call parent test.
        parent::testMustInitializeModuleConfigurationsIfAModuleIsSpecified();

        // Assert test.
        $configurations = self::$class->getStaticPropertyValue('configurations');

        $this->assertIsObject($configurations->myModule);
    }
}
