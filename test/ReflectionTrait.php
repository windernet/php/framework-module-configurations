<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Configurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use ReflectionClass;
use ReflectionException;
use ReflectionMethod;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      TRAIT                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * This trait defines some functionalities regarding reflection, which help execute some tests.
 */
trait ReflectionTrait
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                               TEST PROPERTIES                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * The reflection class of the class, which shall be tested.
     */
    protected static ?ReflectionClass  $class            = null;

    /**
     * The name of the class, which shall be tested.
     */
    protected static ?string           $className        = null;

    /**
     * The reflection method of the method, which shall be tested.
     */
    protected static ?ReflectionMethod $method           = null;

    /**
     * The accessibility of the method, which shall be tested.
     */
    protected static ?bool             $methodAccessible = null;

    /**
     * The name of the method, which shall be tested.
     */
    protected static ?string           $methodName       = null;

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                              PHP UNIT FUNCTIONS                                                              \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Prepares all upcoming tests of this test class, before their execution.
     *
     * @throws ReflectionException - If {@see static::$methodName} is invalid.
     */
    public static function setUpBeforeClass(): void
    {
        // Get the name of the class, which shall be tested.
        $testClass = explode('\\', (new ReflectionClass(static::class))->getName());

        unset ($testClass[0], $testClass[count($testClass)]); // Remove prefix ("Test\") and suffix (name of method, which shall be tested).

        static::$className = implode('\\', $testClass);

        unset ($testClass);

        // Get reflection class and method of the class and method, which shall be tested.
        static::$class            = static::$className  ? new ReflectionClass(static::$className)        : null;
        static::$method           = static::$methodName ? static::$class->getMethod(static::$methodName) : null;
        static::$methodAccessible = static::$method?->isPublic() ?? false;

        static::$method?->setAccessible(true);
    }

    /**
     * Cleans up after all tests of this test class have been executed.
     */
    public static function tearDownAfterClass(): void
    {
        // Restore accessibility of reflection method.
        static::$method?->setAccessible(static::$methodAccessible);

        // Unset all properties to free memory for upcoming tests.
        static::$class            = null;
        static::$className        = null;
        static::$method           = null;
        static::$methodAccessible = null;
        static::$methodName       = null;
    }
}
