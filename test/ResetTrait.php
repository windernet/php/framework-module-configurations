<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-configurations/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Configurations;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use ReflectionException;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      TRAIT                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * This trait assures, that the static properties of the class, which shall be tested, are uninitialized before running a test.
 */
trait ResetTrait
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                 USED TRAITS                                                                  \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    use ReflectionTrait {
        setUpBeforeClass   as __setUpBeforeClass;
        tearDownAfterClass as __tearDownAfterClass;
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                               TEST PROPERTIES                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * All static properties of the class, which shall be tested, and their initial values.
     */
    protected static ?array $properties = null;

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                              PHP UNIT FUNCTIONS                                                              \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Prepares all upcoming tests of this test class, before their execution.
     *
     * @throws ReflectionException - If {@see static::$methodName} is invalid.
     */
    public static function setUpBeforeClass(): void
    {
        // Set up traits.
        static::__setupBeforeClass();

        // Get all static properties and their initial values of the class, which shall be tested.
        static::$properties = static::$class->getStaticProperties();
    }

    /**
     * Cleans up after all tests of this test class have been executed.
     */
    public static function tearDownAfterClass(): void
    {
        // Unset all properties to free memory for upcoming tests.
        static::$properties = null;

        // Tear down traits.
        static::__tearDownAfterClass();
    }

    /**
     * Prepares a test, before its execution.
     */
    public function setUp(): void
    {
        // Make sure, static class properties are not initialized.
        foreach (static::$properties as $propertyName => $propertyValue) {
            $this->assertEquals($propertyValue, static::$class->getStaticPropertyValue($propertyName));
        }
    }

    /**
     * Cleans up after a test has been executed.
     */
    public function tearDown(): void
    {
        // Reset static class properties.
        foreach (static::$properties as $propertyName => $propertyValue) {
            static::$class->setStaticPropertyValue($propertyName, $propertyValue);
        }
    }
}
